Begin3
Language:    FR, 850, French (Standard)
Title:       FDAPM
Description: Outil de gestion d'�nergie avanc�
Summary:     Contr�le/information APM/ACPI, contr�le de TSR/�conomie d'�nergie, vidage du tampon, red�marrage et plus encore.
Keywords:    APM, ACPI, alimentation, batterie, �conomie d'�nergie, ralentissement, �cran dpms, red�marrage, arr�t
End
